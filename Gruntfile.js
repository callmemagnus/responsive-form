module.exports = function(grunt) {

	var configuration = {
		pkg: grunt.file.readJSON('package.json'),
		less: {
			develop: {
				options : {
					compress: false,
					ieCompat: true,
					banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
				},
				files : {
					"out/all.css": "src/less/all.less"
				}
			}
		},
		watch: {
			"style" : {
		  		files: ['src/less/*.less'],
		  		tasks: ['less:develop']
		  	},
		  	"pages" : {
		  		files: ['src/html/*.html'],
		  		tasks: ['copy']
		  	},
		},
		copy: {
  			html: {
  				files: [
  					{ expand: true, cwd: 'src/html', src: ['**'], dest: 'out/', filter: 'isFile'}
				]
			}
		},
		clean: {
			files: ['out']
		}
	};

	grunt.initConfig(configuration);

	grunt.loadNpmTasks('grunt-contrib-clean')
	grunt.loadNpmTasks('grunt-contrib-uglify');
	// grunt.loadNpmTasks('grunt-contrib-jshint');
	// grunt.loadNpmTasks('grunt-contrib-qunit');
	grunt.loadNpmTasks('grunt-contrib-watch');
	// grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.registerTask('test', ['jshint', 'qunit']);

	grunt.registerTask('dev', ['clean', 'less:develop', 'copy:html']);

	grunt.registerTask('default', ['dev']);
  // grunt.registerTask('default', ['jshint', 'qunit', 'concat', 'uglify']);

};