# Responsive Form

This is a simple form, built on mobile first principle.

All css rules are made for mobile and then modified for larger screens.

## Build

1. install npm and grunt
2. `npm install`
3. grunt

Then you can check the output in `out` folder.

    cd out
    python -m SimpleHttpModule

to view the result.